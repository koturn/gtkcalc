#define _USE_MATH_DEFINES
// C headers
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstring>
// C++ headers
#include <iostream>
#include <limits>
#include <stack>
#include <string>
#include <vector>
// GUI Library headers
#include <gtk/gtk.h>

#include "FormulaToken.hpp"
#include "FormulaParser.hpp"


#define TYPESIZE(type) pow(2,sizeof(type))

GtkWidget* mainWindow;

typedef struct main_dialog_type {
	//ウィンドウ
	GtkWidget *window;
	//ボタン
	GtkWidget *btn_Calc;//計算開始ボタン(=)
	GtkWidget *btn_Quit;//アプリケーション終了ボタン
	GtkWidget *btn_Ad;//足し算
	GtkWidget *btn_Sb;//引き算
	GtkWidget *btn_Ml;//掛け算
	GtkWidget *btn_Dv;//割り算
	GtkWidget *btn_Sp;//剰余算
	//GtkWidget *btn_Sr;//平方根
	//GtkWidget *btn_Sq;//平方
	GtkWidget *btn_StartBrackets;
	GtkWidget *btn_EndBrackets;
	GtkWidget *btn_Fr;//分数
	GtkWidget *btn_AS;//±
	GtkWidget *btn_Dp;//小数点
	GtkWidget *btn_MC;//MC
	GtkWidget *btn_MR;//MR
	GtkWidget *btn_MP;//M+
	GtkWidget *btn_MM;//M-
	GtkWidget *btn_MS;//MS
	GtkWidget *btn_ML;//MoveLeft
	GtkWidget *btn_CE;//CE（最後尾削除）
	GtkWidget *btn_CLR;//Clear
	GtkWidget *btn0;
	GtkWidget *btn1;
	GtkWidget *btn2;
	GtkWidget *btn3;
	GtkWidget *btn4;
	GtkWidget *btn5;
	GtkWidget *btn6;
	GtkWidget *btn7;
	GtkWidget *btn8;
	GtkWidget *btn9;

	GtkWidget *menu;//プルダウンメニュー
	GtkWidget *entry;//入力（兼表示）フォーム
	GtkWidget *vbox;//VerticalBox。垂直方向・縦一列
	GtkWidget *hbox;//HorizontalBox。水平方向・横一列


	std::string formula;
	bool resultDisplaying;//結果表示中かどうか//formulaの1つ目に値が格納されているので、それを上書きする処理に変更させる。下のentryDigitも同じ
	bool entryNotFormla;//「一部のボタンは～」が表示中かどうか//数字が格納されていないとして、「0」を頭に入れないようにもできる。
}MainDialog;


//関数定義
static void BtnUpdateFormula(GtkWidget*, gpointer);
static void KeyUpdateFormula(GtkWidget*, GdkEventKey*, gpointer);
static void clearFourmula(GtkWidget*, gpointer);
static void display(GtkWidget*, const std::string&);
static double eval(const std::vector<FormulaToken>& rpnStack);
static void calc(GtkWidget*, gpointer);
static bool CompareOperator(char, char);
static int setRankToOpr(char);


#ifndef _MSC_VER
template<typename... Args>
static inline int
sprintf_s(char* dst, const char* fmt, Args&&... args) noexcept
{
	return std::sprintf(dst, fmt, std::forward<Args>(args)...);
}
#endif


//要素や構成を設定・受け取る大枠を定義
MainDialog dialog;

//メイン
int main(int argc, char** argv) {
	//GTK+の初期化とオプション解析
	gtk_init(&argc, &argv);

	gchar *gLabel;


	//dialog.serialNum = FALSE;
	//dialog.floatingPoiinted = FALSE;
	dialog.resultDisplaying = false;
	dialog.entryNotFormla = false;


	//ウィンドウの作成
	dialog.window = gtk_window_new(GTK_WINDOW_TOPLEVEL);//ウィンドウのインスタンス
	gtk_widget_set_size_request(dialog.window, 320, 450);//ウィンドウ初期サイズの設定
	gLabel = g_locale_to_utf8("電卓", -1, nullptr, nullptr, nullptr);
	gtk_window_set_title(GTK_WINDOW(dialog.window), gLabel);//ウィンドウタイトルの設定
	gtk_window_set_position(GTK_WINDOW(dialog.window), GTK_WIN_POS_CENTER);//起動時のウィンドウの表示位置の設定
	gtk_container_set_border_width(GTK_CONTAINER(dialog.window), 5);//（ボタンなどの）子要素からの幅（＝padding？）の設定？第2引数でpadding数値指定している？

	//子要素のレイアウト（縦方向）設定
	dialog.vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);//縦方向に<第2引数>px間隔で並べる。
	gtk_container_add(GTK_CONTAINER(dialog.window), dialog.vbox);//ウィンドウに追加

	//1行目の子要素群
	dialog.hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);//行要素の生成//子要素を横方向に<第2引数>px間隔で並べる。
	gtk_box_pack_start(GTK_BOX(dialog.vbox), dialog.hbox, FALSE, FALSE, 0);//行要素を縦空間に設置
	//子要素
	dialog.menu = gtk_combo_box_text_new();//プルダウンメニューフォームのインスタンス生成
	//プルダウンメニューの項目追加
	gLabel= g_locale_to_utf8("標準", -1, nullptr, nullptr, nullptr);
	gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(dialog.menu), 0, nullptr, gLabel);
	gLabel = g_locale_to_utf8("関数電卓", -1, nullptr, nullptr, nullptr);
	gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(dialog.menu), 1, nullptr, gLabel);
	gLabel = g_locale_to_utf8("プログラマー", -1, nullptr, nullptr, nullptr);
	gtk_combo_box_text_insert(GTK_COMBO_BOX_TEXT(dialog.menu), 2, nullptr, gLabel);
	//プルダウンメニューのデフォルト選択を0(="+")に設定。
	gtk_combo_box_set_active(GTK_COMBO_BOX(dialog.menu), 0);//第2引数で設定。
	gtk_widget_set_size_request(dialog.menu, 50,35);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.menu, FALSE, TRUE, 0);//上で構成したフォームを設置。


	//2行目の子要素群
	//枠組み構築
	dialog.hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);//行要素の生成
	gtk_box_pack_start(GTK_BOX(dialog.vbox), dialog.hbox, FALSE, FALSE, 0);//行要素を縦空間に設置
	//子要素
	dialog.entry = gtk_entry_new();//入力フォームのインスタンス生成（Entryを使っているけど、今回は入力フォームとしては使わない。実質Label）
	gLabel = g_locale_to_utf8("一部のボタンは使えません", -1, nullptr, nullptr, nullptr);
	gtk_entry_set_text(GTK_ENTRY(dialog.entry),gLabel);
	gtk_widget_set_size_request(dialog.entry, 298, 105);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.entry, TRUE, TRUE, 0);//上で構成したフォームを設置。


	//3行目の子要素群
	dialog.hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_set_size_request(dialog.hbox, 300, 30);
	gtk_box_pack_start(GTK_BOX(dialog.vbox), dialog.hbox, FALSE, FALSE, 0);
	//子要素
	dialog.btn_MC = gtk_button_new_with_label("MC");
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_MC, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_MC, 60, 30);
	//g_signal_connect(dialog.btn_MC, "clicked", G_CALLBACK(), nullptr);
	dialog.btn_MR = gtk_button_new_with_label("MR");
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_MR, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_MR, 60, 30);
	//g_signal_connect(dialog.btn_MR, "clicked", G_CALLBACK(), nullptr);
	dialog.btn_MP = gtk_button_new_with_label("M+");
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_MP, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_MP, 60, 30);
	//g_signal_connect(dialog.btn_MP, "clicked", G_CALLBACK(), nullptr);
	dialog.btn_MM = gtk_button_new_with_label("M-");
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_MM, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_MM, 60, 30);
	//g_signal_connect(dialog.btn_MM, "clicked", G_CALLBACK(), nullptr);
	dialog.btn_MS = gtk_button_new_with_label("MS");
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_MS, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_MS, 60, 30);
	//g_signal_connect(dialog.btn_MS, "clicked", G_CALLBACK(), nullptr);


	//4行目の子要素群
	dialog.hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_set_size_request(dialog.hbox, 300, 40);
	gtk_box_pack_start(GTK_BOX(dialog.vbox), dialog.hbox, FALSE, FALSE,0);
	//子要素
	gLabel = g_locale_to_utf8("％", -1, nullptr, nullptr, nullptr);
	dialog.btn_Sp = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_Sp, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_Sp, 75, 40);
	g_signal_connect(dialog.btn_Sp, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn_Sp);
	gLabel = g_locale_to_utf8("（", -1, nullptr, nullptr, nullptr);
	dialog.btn_StartBrackets = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_StartBrackets, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_StartBrackets, 75, 40);
	g_signal_connect_swapped(dialog.btn_StartBrackets, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn_StartBrackets);
	gLabel = g_locale_to_utf8("）", -1, nullptr, nullptr, nullptr);
	dialog.btn_EndBrackets = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_EndBrackets, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_EndBrackets, 75, 40);
	g_signal_connect_swapped(dialog.btn_EndBrackets, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn_EndBrackets);
	gLabel = g_locale_to_utf8("1/x", -1, nullptr, nullptr, nullptr);
	dialog.btn_Fr = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_Fr, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_Fr, 75, 40);
	//g_signal_connect_swapped(dialog.btn_Fr, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn_Fr);


	//5行目の子要素群
	dialog.hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_set_size_request(dialog.hbox, 300, 40);
	gtk_box_pack_start(GTK_BOX(dialog.vbox), dialog.hbox, FALSE, FALSE, 0);
	//子要素
	gLabel = g_locale_to_utf8("CE", -1, nullptr, nullptr, nullptr);
	dialog.btn_CE = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_CE, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_CE, 75, 40);
	g_signal_connect(dialog.btn_CE, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn_CE);
	gLabel = g_locale_to_utf8("C", -1, nullptr, nullptr, nullptr);
	dialog.btn_CLR = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_CLR, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_CLR, 75, 40);
	g_signal_connect(dialog.btn_CLR, "clicked", G_CALLBACK(clearFourmula), nullptr);
	gLabel = g_locale_to_utf8("←", -1, nullptr, nullptr, nullptr);
	dialog.btn_ML = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_ML, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_ML, 75, 40);
	//g_signal_connect_swapped(dialog.btn_ML, "clicked", G_CALLBACK(), nullptr);
	gLabel = g_locale_to_utf8("÷", -1, nullptr, nullptr, nullptr);
	dialog.btn_Dv = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_Dv, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_Dv, 75, 40);
	g_signal_connect_swapped(dialog.btn_Dv, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn_Dv);


	//6行目の子要素群
	dialog.hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_set_size_request(dialog.hbox, 300, 40);
	gtk_box_pack_start(GTK_BOX(dialog.vbox), dialog.hbox, FALSE, FALSE, 0);
	//子要素
	gLabel = g_locale_to_utf8("７", -1, nullptr, nullptr, nullptr);
	dialog.btn7 = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn7, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn7, 75, 40);
	g_signal_connect_swapped(dialog.btn7, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn7);
	gLabel = g_locale_to_utf8("８", -1, nullptr, nullptr, nullptr);
	dialog.btn8 = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn8, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn8, 75, 40);
	g_signal_connect_swapped(dialog.btn8, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn8);
	gLabel = g_locale_to_utf8("９", -1, nullptr, nullptr, nullptr);
	dialog.btn9 = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn9, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn9, 75, 40);
	g_signal_connect_swapped(dialog.btn9, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn9);
	gLabel = g_locale_to_utf8("×" ,-1, nullptr, nullptr, nullptr);
	dialog.btn_Ml = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_Ml, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_Ml, 75, 40);
	g_signal_connect_swapped(dialog.btn_Ml, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn_Ml);


	//7行目の子要素群
	dialog.hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_set_size_request(dialog.hbox, 300, 40);
	gtk_box_pack_start(GTK_BOX(dialog.vbox), dialog.hbox, FALSE, FALSE, 0);
	//子要素
	gLabel = g_locale_to_utf8("４", -1, nullptr, nullptr, nullptr);
	dialog.btn4 = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn4, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn4, 75, 40);
	g_signal_connect_swapped(dialog.btn4, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn4);
	gLabel = g_locale_to_utf8("５", -1, nullptr, nullptr, nullptr);
	dialog.btn5 = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn5, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn5, 75, 40);
	g_signal_connect_swapped(dialog.btn5, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn5);
	gLabel = g_locale_to_utf8("６", -1, nullptr, nullptr, nullptr);
	dialog.btn6 = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn6, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn6, 75, 40);
	g_signal_connect_swapped(dialog.btn6, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn6);
	gLabel = g_locale_to_utf8("―", -1, nullptr, nullptr, nullptr);
	dialog.btn_Sb = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_Sb, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_Sb, 75, 40);
	g_signal_connect_swapped(dialog.btn_Sb, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn_Sb);


	//8行目の子要素群
	dialog.hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_set_size_request(dialog.hbox, 300, 40);
	gtk_box_pack_start(GTK_BOX(dialog.vbox), dialog.hbox, FALSE, FALSE, 0);
	//子要素
	gLabel = g_locale_to_utf8("１", -1, nullptr, nullptr, nullptr);
	dialog.btn1 = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn1, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn1, 75, 40);
	g_signal_connect_swapped(dialog.btn1, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn1);
	gLabel = g_locale_to_utf8("２", -1, nullptr, nullptr, nullptr);
	dialog.btn2 = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn2, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn2, 75, 40);
	g_signal_connect_swapped(dialog.btn2, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn2);
	gLabel = g_locale_to_utf8("３", -1, nullptr, nullptr, nullptr);
	dialog.btn3 = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn3, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn3, 75, 40);
	g_signal_connect_swapped(dialog.btn3, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn3);
	gLabel = g_locale_to_utf8("＋", -1, nullptr, nullptr, nullptr);
	dialog.btn_Ad = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_Ad, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_Ad, 75, 40);
	g_signal_connect_swapped(dialog.btn_Ad, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn_Ad);


	//9行目の子要素群
	dialog.hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_set_size_request(dialog.hbox, 300, 40);
	gtk_box_pack_start(GTK_BOX(dialog.vbox), dialog.hbox, FALSE, FALSE, 0);
	//子要素
	gLabel = g_locale_to_utf8("±", -1, nullptr, nullptr, nullptr);
	dialog.btn_AS = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_AS, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_AS, 75, 40);
	//g_signal_connect_swapped(dialog.btn_AS, "clicked", G_CALLBACK(), nullptr);
	gLabel = g_locale_to_utf8("０", -1, nullptr, nullptr, nullptr);
	dialog.btn0 = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn0, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn0, 75, 40);
	g_signal_connect_swapped(dialog.btn0, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn0);
	gLabel = g_locale_to_utf8("．", -1, nullptr, nullptr, nullptr);
	dialog.btn_Dp = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_Dp, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_Dp, 75, 40);
	g_signal_connect_swapped(dialog.btn_Dp, "clicked", G_CALLBACK(BtnUpdateFormula), dialog.btn_Dp);
	gLabel = g_locale_to_utf8("＝", -1, nullptr, nullptr, nullptr);
	dialog.btn_Calc = gtk_button_new_with_label(gLabel);
	gtk_box_pack_start(GTK_BOX(dialog.hbox), dialog.btn_Calc, TRUE, TRUE, 0);
	gtk_widget_set_size_request(dialog.btn_Calc, 75, 40);
	g_signal_connect(dialog.btn_Calc, "clicked", G_CALLBACK(calc), nullptr);


	//キー操作
	g_signal_connect(G_OBJECT(dialog.entry), "key-press-event", G_CALLBACK(KeyUpdateFormula), nullptr);

	//ウィンドウ右上の「×」ボタンにおけるアクション
	g_signal_connect(dialog.window, "destroy", G_CALLBACK(gtk_main_quit), nullptr);


	//上記で（ウィジェット＝要素群を）設定して構成が出来上がった大枠を表示する。
	gtk_widget_show_all(dialog.window);

	//GTK+のメイン関数を呼び出してループ実現。
	gtk_main();

	return 0;
}







//ボタンによる式の更新
static void
BtnUpdateFormula(GtkWidget*, gpointer data)
{
	if (dialog.btn0 == data) {
		if (dialog.resultDisplaying) {
			//何もしない
		} else if (dialog.entryNotFormla) {
			//何もしない
		} else {
			dialog.formula.push_back('0');
		}
	} else if (dialog.btn1 == data) {
		if (dialog.resultDisplaying) {
			dialog.formula = "1";
			dialog.resultDisplaying = false;
		} else if (dialog.entryNotFormla) {
			dialog.formula = "1";
			dialog.entryNotFormla = false;
		} else {
			dialog.formula.push_back('1');
		}
	} else if (dialog.btn2 == data) {
		if (dialog.resultDisplaying) {
			dialog.formula = "2";
			dialog.resultDisplaying = false;
		} else if (dialog.entryNotFormla) {
			dialog.formula = "2";
			dialog.entryNotFormla = false;
		} else {
			dialog.formula.push_back('2');
		}
	} else if (dialog.btn3 == data) {
		if (dialog.resultDisplaying) {
			dialog.formula = "3";
			dialog.resultDisplaying = false;
		} else if (dialog.entryNotFormla) {
			dialog.formula = "3";
			dialog.entryNotFormla = false;
		} else {
			dialog.formula.push_back('3');
		}
	} else if (dialog.btn4 == data) {
		if (dialog.resultDisplaying) {
			dialog.formula = "4";
			dialog.resultDisplaying = false;
		} else if (dialog.entryNotFormla) {
			dialog.formula = "4";
			dialog.entryNotFormla = false;
		} else {
			dialog.formula.push_back('4');
		}
	} else if (dialog.btn5 == data) {
		if (dialog.resultDisplaying) {
			dialog.formula = "5";
			dialog.resultDisplaying = false;
		} else if (dialog.entryNotFormla) {
			dialog.formula = "5";
			dialog.entryNotFormla = false;
		} else {
			dialog.formula.push_back('5');
		}
	} else if (dialog.btn6 == data) {
		if (dialog.resultDisplaying) {
			dialog.formula = "6";
			dialog.resultDisplaying = false;
		} else if (dialog.entryNotFormla) {
			dialog.formula = "6";
			dialog.entryNotFormla = false;
		} else {
			dialog.formula.push_back('6');
		}
	} else if (dialog.btn7 == data) {
		if (dialog.resultDisplaying) {
			dialog.formula = "7";
			dialog.resultDisplaying = false;
		} else if (dialog.entryNotFormla) {
			dialog.formula = "7";
			dialog.entryNotFormla = false;
		} else {
			dialog.formula.push_back('7');
		}
	} else if (dialog.btn8 == data) {
		if (dialog.resultDisplaying) {
			dialog.formula = "8";
			dialog.resultDisplaying = false;
		} else if (dialog.entryNotFormla) {
			dialog.formula = "8";
			dialog.entryNotFormla = false;
		} else {
			dialog.formula.push_back('8');
		}
	} else if (dialog.btn9 == data) {
		if (dialog.resultDisplaying) {
			dialog.formula = "9";
			dialog.resultDisplaying = false;
		} else if (dialog.entryNotFormla) {
			dialog.formula = "9";
			dialog.entryNotFormla = false;
		} else {
			dialog.formula.push_back('9');
		}
	} else if (dialog.btn_Ad == data) {  //足し算
		if (dialog.resultDisplaying) {
			//何もしない
		} else if (dialog.entryNotFormla) {
			//何もしない
		} else {
			dialog.formula.push_back('+');
		}
	} else if (dialog.btn_Sb == data) {  //引き算
		if (dialog.resultDisplaying) {
			//何もしない
		} else if (dialog.entryNotFormla) {
			//何もしない
		} else {
			dialog.formula.push_back('-');
		}
	} else if (dialog.btn_Ml == data) {  //掛け算
		if (dialog.resultDisplaying) {
			//何もしない
		} else if (dialog.entryNotFormla) {
			//何もしない
		} else {
			dialog.formula.push_back('*');
		}
	} else if (dialog.btn_Dv == data) {  //割り算
		if (dialog.resultDisplaying) {
			//何もしない
		} else if (dialog.entryNotFormla) {
			//何もしない
		} else {
			dialog.formula.push_back('/');
		}
	} else if (dialog.btn_Sp == data) {  //剰余算
		if (dialog.resultDisplaying) {
			//何もしない
		} else if (dialog.entryNotFormla) {
			//何もしない
		} else {
			dialog.formula.push_back('%');
		}
	} else if (dialog.btn_StartBrackets == data) {  //かっこ開始
		if (dialog.resultDisplaying) {
			dialog.formula = "(";
			dialog.resultDisplaying = false;
		} else if (dialog.entryNotFormla) {
			dialog.formula = "(";
			dialog.entryNotFormla = false;
		} else {
			dialog.formula.push_back('(');
		}
	} else if (dialog.btn_EndBrackets == data) {  //かっこ閉じ
		if (dialog.resultDisplaying) {
			//何もしない
		} else if (dialog.entryNotFormla) {
			//何もしない
		} else {
			dialog.formula.push_back(')');
		}
	} else if (dialog.btn_Dp == data) {  //小数点
		if (dialog.resultDisplaying) {
			//何もしない
		} else if (dialog.entryNotFormla) {
			//何もしない
		} else {
			dialog.formula.push_back('.');
		}
	} else if (dialog.btn_CE == data) {  //最後尾削除
		if (dialog.resultDisplaying || (dialog.formula.size() == 1)) {
			gchar *newLabel;

			newLabel = g_locale_to_utf8("一部のボタンは使えません", -1, nullptr, nullptr, nullptr);
			dialog.formula = std::string(newLabel);

			dialog.entryNotFormla = TRUE;
		} else {
			dialog.formula.pop_back();
		}
	}

	for (std::size_t i = 0; i < dialog.formula.size(); i++) {
		std::printf("%zu:%s\n", i, dialog.formula.c_str());
	}

	display(dialog.entry, dialog.formula);
}



//キー入力による式の更新
static void KeyUpdateFormula(GtkWidget*, GdkEventKey* event, gpointer) {
	//「event->keyval==???」で使用するパラメータのみ設定。なお第6回のhomeWork2でキーの番号を把握できる
	switch (event->keyval) {
		case '0':
			if (dialog.resultDisplaying) {
				//何もしない
			} else if (dialog.entryNotFormla) {
				//何もしない
			} else {
				dialog.formula.push_back('0');
			}
			break;
		case '1':
			if (dialog.resultDisplaying) {
				dialog.formula = "1";
				dialog.resultDisplaying = false;
			} else if (dialog.entryNotFormla) {
				dialog.formula = "1";
				dialog.entryNotFormla = false;
			} else {
				dialog.formula.push_back('1');
			}
			break;
		case '2':
			if (dialog.resultDisplaying) {
				dialog.formula = "2";
				dialog.resultDisplaying = false;
			} else if (dialog.entryNotFormla) {
				dialog.formula = "2";
				dialog.entryNotFormla = false;
			} else {
				dialog.formula.push_back('2');
			}
			break;
		case '3':
			if (dialog.resultDisplaying) {
				dialog.formula = "3";
				dialog.resultDisplaying = false;
			} else if (dialog.entryNotFormla) {
				dialog.formula = "3";
				dialog.entryNotFormla = false;
			} else {
				dialog.formula.push_back('3');
			}
			break;
		case '4':
			if (dialog.resultDisplaying) {
				dialog.formula = "4";
				dialog.resultDisplaying = false;
			} else if (dialog.entryNotFormla) {
				dialog.formula = "4";
				dialog.entryNotFormla = false;
			} else {
				dialog.formula.push_back('4');
			}
			break;
		case '5':
			if (dialog.resultDisplaying) {
				dialog.formula = "5";
				dialog.resultDisplaying = false;
			} else if (dialog.entryNotFormla) {
				dialog.formula = "5";
				dialog.entryNotFormla = false;
			} else {
				dialog.formula.push_back('5');
			}
			break;
		case '6':
			if (dialog.resultDisplaying) {
				dialog.formula = "6";
				dialog.resultDisplaying = false;
			} else if (dialog.entryNotFormla) {
				dialog.formula = "6";
				dialog.entryNotFormla = false;
			} else {
				dialog.formula.push_back('6');
			}
			break;
		case '7':
			if (dialog.resultDisplaying) {
				dialog.formula = "7";
				dialog.resultDisplaying = false;
			} else if (dialog.entryNotFormla) {
				dialog.formula = "7";
				dialog.entryNotFormla = false;
			} else {
				dialog.formula.push_back('7');
			}
			break;
		case '8':
			if (dialog.resultDisplaying) {
				dialog.formula = "8";
				dialog.resultDisplaying = false;
			}
			else if (dialog.entryNotFormla) {
				dialog.formula = "8";
				dialog.entryNotFormla = false;
			}
			else {
				dialog.formula.push_back('8');
			}
			break;
		case '9':
			if (dialog.resultDisplaying) {
				dialog.formula = "9";
				dialog.resultDisplaying = false;
			}
			else if (dialog.entryNotFormla) {
				dialog.formula = "9";
				dialog.entryNotFormla = false;
			}
			else {
				dialog.formula.push_back('9');
			}
			break;
		case '+':
			if (dialog.resultDisplaying) {
				//何もしない
			} else if (dialog.entryNotFormla) {
				//何もしない
			} else {
				dialog.formula.push_back('+');
			}
			break;
		case '-':
			if (dialog.resultDisplaying) {
				//何もしない
			} else if (dialog.entryNotFormla) {
				//何もしない
			} else {
				dialog.formula.push_back('-');
			}
			break;
		case '*':
			if (dialog.resultDisplaying) {
				//何もしない
			} else if (dialog.entryNotFormla) {
				//何もしない
			} else {
				dialog.formula.push_back('*');
			}
			break;
		case '/':
			if (dialog.resultDisplaying) {
				//何もしない
			} else if (dialog.entryNotFormla) {
				//何もしない
			} else {
				dialog.formula.push_back('/');
			}
			break;
		case '%':
			if (dialog.resultDisplaying) {
				//何もしない
			} else if (dialog.entryNotFormla) {
				//何もしない
			} else {
				dialog.formula.push_back('%');
			}
			break;
		case '(':
			if (dialog.resultDisplaying) {
				dialog.formula = "(";
				dialog.resultDisplaying = false;
			} else if (dialog.entryNotFormla) {
				dialog.formula = "(";
				dialog.entryNotFormla = false;
			} else {
				dialog.formula.push_back('(');
			}
			break;
		case ')':
			if (dialog.resultDisplaying) {
				//何もしない
			} else if (dialog.entryNotFormla) {
				//何もしない
			} else {
				dialog.formula.push_back(')');
			}
			break;
		case '.':
			if (dialog.resultDisplaying) {
				//何もしない
			} else if (dialog.entryNotFormla) {
				//何もしない
			} else {
				dialog.formula.push_back('.');
			}
			break;
		case 65288:  //最後尾削除
			if (dialog.resultDisplaying || (dialog.formula.size() == 1)) {
				gchar *newLabel;
				newLabel = g_locale_to_utf8("一部のボタンは使えません", -1, nullptr, nullptr, nullptr);
				dialog.formula = std::string(newLabel);
				dialog.entryNotFormla = true;
			} else {
				dialog.formula.pop_back();
			}
			break;
	}

	for (std::size_t i = 0; i < dialog.formula.size(); i++) {
		std::printf("%zu:%s\n", i, dialog.formula.c_str());
	}

	display(dialog.entry, dialog.formula);
	//最後に。display(dialog.entry , dialog.formula);
}


//式のクリア
static void clearFourmula(GtkWidget*, gpointer) {
	if (!dialog.entryNotFormla) {
		//formulaのうち0番目以外を削除する
		dialog.formula.resize(1);

		gchar *newLabel;
		//この後に数字が表示されていたら再度「C」
		newLabel = g_locale_to_utf8("一部のボタンは使えません", -1, nullptr, nullptr, nullptr);
		dialog.formula = std::string(newLabel);

		dialog.entryNotFormla = true;


		display(dialog.entry, dialog.formula);
	}
}

//式・結果の表示
static void display(GtkWidget* entry, const std::string& formula) {
	std::printf("%s\n", formula.c_str());
	gtk_entry_set_text(GTK_ENTRY(entry), formula.c_str());
}


static double
eval(const std::vector<FormulaToken>& rpnStack)
{
	std::stack<double> operandStack;

	for (const auto &e : rpnStack) {
		switch (e.getType()) {
			case FormulaToken::Type::kInteger:
				operandStack.push(static_cast<double>(e.getIntValue()));
				break;
			case FormulaToken::Type::kFloat:
				operandStack.push(e.getFloatValue());
				break;
			case FormulaToken::Type::kOperator:
				if (operandStack.size() < 2) {
					throw std::runtime_error("Too many operators");
				}
				auto rhs = std::move(operandStack.top());
				operandStack.pop();
				auto lhs = std::move(operandStack.top());
				operandStack.pop();
				switch (e.getOperator()) {
					case '+':
						operandStack.push(lhs + rhs);
						break;
					case '-':
						operandStack.push(lhs - rhs);
						break;
					case '*':
						operandStack.push(lhs * rhs);
						break;
					case '/':
						operandStack.push(lhs / rhs);
						break;
					case '%':
						operandStack.push(static_cast<double>(static_cast<int>(lhs) % static_cast<int>(rhs)));
						break;
					default:
						throw std::runtime_error("Invalid operator: " +  std::string(1, e.getOperator()));
				}
				break;
		}
	}
	if (operandStack.size() != 1) {
		std::cout << "Invalid size: " << operandStack.size() << std::endl;
	}
	return operandStack.top();
}


//計算
static void
calc(GtkWidget*, gpointer)
{
	auto& formula = dialog.formula;
	try {
		FormulaParser fp;
		auto result = eval(fp.parse(dialog.formula));
		formula = std::to_string(result);
		display(dialog.entry, formula);
		dialog.resultDisplaying = true;
	} catch (const std::runtime_error& ex) {
		std::cerr << ex.what() << std::endl;
		formula = std::string(g_locale_to_utf8("計算に失敗しました", -1, nullptr, nullptr, nullptr));
		display(dialog.entry, formula);
		dialog.entryNotFormla = true;
	}
}


//2つの記号（演算子）の優先度の判定//1つ目の引数の記号（スタックに入っている直前の記号）>=2つ目（新しい記号）ならばtrue
static bool
CompareOperator(char op1, char op2)
{
	return setRankToOpr(op1) >= setRankToOpr(op2);
}


//記号の優先度ランクを割り当てる
static int
setRankToOpr(char op)
{
	if (op == '+' || op == '-') {
		return 1;
	} else if (op == '*' || op == '/' || op == '*') {
		return 2;
	} else {
		std::cout << "Invalid root" << std::endl;
		return std::numeric_limits<int>::max();
	}
}
