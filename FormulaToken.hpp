#ifndef FORMULA_TOKEN_HPP
#define FORMULA_TOKEN_HPP


#include <iostream>


class FormulaToken
{
public:
	enum class Type
	{
		kEof,
		kParseError,
		kInteger,
		kFloat,
		kOperator,
		kLeftParen,
		kRightParen,
	};

	FormulaToken(FormulaToken::Type type) noexcept
		: m_type(type)
		, m_doubleValue()
	{}

	FormulaToken(char op) noexcept
		: m_type(FormulaToken::Type::kOperator)
		, m_op(op)
	{}

	FormulaToken(int op) noexcept
		: m_type(FormulaToken::Type::kInteger)
		, m_intValue(op)
	{}

	FormulaToken(double op) noexcept
		: m_type(FormulaToken::Type::kFloat)
		, m_doubleValue(op)
	{}

	FormulaToken(const char* errmsg) noexcept
		: m_type(FormulaToken::Type::kParseError)
		, m_errmsg(errmsg)
	{}

	FormulaToken::Type
	getType() const noexcept
	{
		return m_type;
	}

	const char*
	getErrorMessagePtr() const noexcept
	{
		return m_errmsg;
	}

	bool
	isEof() const noexcept
	{
		return m_type == FormulaToken::Type::kEof;
	}

	char
	getOperator() const noexcept
	{
		return m_op;
	}

	int
	getIntValue() const noexcept
	{
		switch (m_type) {
			case FormulaToken::Type::kInteger:
				return m_intValue;
			case FormulaToken::Type::kFloat:
				return static_cast<int>(m_doubleValue);
			case FormulaToken::Type::kOperator:
				return static_cast<int>(m_op);
			default:
				return 0;
		}
	}

	double
	getFloatValue() const noexcept
	{
		switch (m_type) {
			case FormulaToken::Type::kInteger:
				return static_cast<double>(m_intValue);
			case FormulaToken::Type::kFloat:
				return m_doubleValue;
			case FormulaToken::Type::kOperator:
				return static_cast<double>(m_op);
			default:
				return 0.0;
		}
	}

	template <typename CharT, typename Traits>
	friend std::basic_ostream<CharT, Traits>&
	operator<<(std::basic_ostream<CharT, Traits>& os, const FormulaToken& this_) noexcept
	{
		switch (this_.getType()) {
			case FormulaToken::Type::kEof:
				os << "[EOF]";
				break;
			case FormulaToken::Type::kParseError:
				os << this_.m_errmsg;
				break;
			case FormulaToken::Type::kInteger:
				os << this_.getIntValue();
				break;
			case FormulaToken::Type::kFloat:
				os << this_.getFloatValue();
				break;
			case FormulaToken::Type::kOperator:
				os << this_.getOperator();
				break;
			case FormulaToken::Type::kLeftParen:
				os << '(';
				break;
			case FormulaToken::Type::kRightParen:
				os << ')';
				break;
		}
		return os;
	}

private:
	FormulaToken::Type m_type;
	union
	{
		char m_op;
		int m_intValue;
		double m_doubleValue;
		const char* m_errmsg;
	};
};


#endif  // FORMULA_TOKEN_HPP
