#ifndef FORMULA_PARSER_HPP
#define FORMULA_PARSER_HPP

#include <cctype>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <utility>
#include <vector>
#include "FormulaToken.hpp"


class FormulaParser
{
public:
	FormulaParser() noexcept
		: m_ss()
	{}

	std::vector<FormulaToken>
	parse(const std::string& text)
	{
		m_ss.str(text);
		m_ss.clear(std::ostringstream::goodbit);

		std::vector<FormulaToken> rpnStack;
		std::vector<FormulaToken> tmpStack;
		auto prevType = FormulaToken::Type::kEof;
		for (auto token = getNextToken(); !token.isEof(); token = getNextToken()) {
			switch (token.getType()) {
				case FormulaToken::Type::kParseError:
					throw std::runtime_error(std::string("Parse error: ") + std::string(token.getErrorMessagePtr()));
				case FormulaToken::Type::kInteger:
				case FormulaToken::Type::kFloat:
					if (prevType == FormulaToken::Type::kInteger || prevType == FormulaToken::Type::kFloat) {
						throw std::runtime_error("Parse error: Sequencial numbers");
					}
					rpnStack.push_back(std::move(token));
					break;
				case FormulaToken::Type::kOperator:
					if (tmpStack.empty()) {
						tmpStack.push_back(std::move(token));
					} else {
						auto opRank = getOperatorRank(token.getOperator());
						do {
							auto& top = tmpStack.back();
							if (opRank > getOperatorRank(top.getOperator())) {
								rpnStack.push_back(std::move(top));
								tmpStack.pop_back();
							} else {
								break;
							}
						} while (!tmpStack.empty());
						tmpStack.push_back(std::move(token));
					}
					break;
				case FormulaToken::Type::kLeftParen:
					tmpStack.push_back(std::move(token));
					break;
				case FormulaToken::Type::kRightParen:
					auto isFoundLeftParen = false;
					for (; !tmpStack.empty(); tmpStack.pop_back()) {
						auto& top = tmpStack.back();
						if (top.getType() == FormulaToken::Type::kLeftParen) {
							isFoundLeftParen = true;
							break;
						}
						rpnStack.push_back(std::move(top));
					}
					if (!isFoundLeftParen) {
						throw std::runtime_error(std::string("Parse error: Left paren not found"));
					}
					break;
			}
		}
		for (; !tmpStack.empty(); tmpStack.pop_back()) {
			rpnStack.push_back(std::move(tmpStack.back()));
		}
		return rpnStack;
	}

private:
	int
	getOperatorRank(char op) noexcept
	{
		switch (op) {
			case '+':
			case '-':
				return 1;
			case '*':
			case '/':
			case '%':
				return 0;
			default:
				return std::numeric_limits<int>::max();
		}
	}

	void
	skipWhitespace() noexcept
	{
		while (std::isspace(m_ss.get()));
		m_ss.unget();
	}

	FormulaToken
	readNumber() noexcept
	{
		auto isFloat = false;
		std::string s;
		for (auto ch = m_ss.get(); ch != EOF; ch = m_ss.get()) {
			if (std::isdigit(ch)) {
				s.push_back(ch);
			} else if (ch == '.') {
				if (isFloat) {
					return FormulaToken("Floating number format error");
				} else {
					isFloat = true;
					s.push_back(ch);
				}
			} else {
				m_ss.unget();
				break;
			}
		}
		try {
			return isFloat ? FormulaToken(std::stod(s)) : FormulaToken(std::stoi(s));
		} catch (const std::invalid_argument& ex) {
			return FormulaToken("Invalid number format");
		} catch (const std::out_of_range& ex) {
			return FormulaToken("Overflow value");
		}
	}

	FormulaToken
	getNextToken() noexcept
	{
		skipWhitespace();
		auto ch = m_ss.get();
		switch (ch) {
			case '+':
				return FormulaToken('+');
			case '-':
				return FormulaToken('-');
			case '*':
				return FormulaToken('*');
			case '/':
				return FormulaToken('/');
			case '%':
				return FormulaToken('%');
			case '(':
				return FormulaToken(FormulaToken::Type::kLeftParen);
			case ')':
				return FormulaToken(FormulaToken::Type::kRightParen);
			case EOF:
				return FormulaToken(FormulaToken::Type::kEof);
			default:
				if (std::isdigit(ch)) {
					m_ss.unget();
					return readNumber();
				} else {
					return FormulaToken(FormulaToken::Type::kParseError);
				}
		}
	}

	std::stringstream m_ss;
};


#endif  // FORMULA_PARSER_HPP
